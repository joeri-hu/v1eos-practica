#!/usr/bin/env bash
ext="${1##*.}"

case "$ext" in
py) python3 "$1";;
sh) bash "$1";;
cc) cat "$1";;
*)  echo "unknown file extension: $ext";;
esac
