#!/usr/bin/env bash
prompt_username () {
    read -p "enter username: " username

    if [ -z "$username" ]; then
        username="$(whoami)"
    fi
}

prompt_password () {
    while true; do
        read -p "$1 password: " password

        if [ ${#password} -ge 8 ]; then
            break
        else
            echo "password too short :("
        fi
    done
}

save_password () {
    echo "$1" > "$3"
    echo -n "$2" | md5sum >> "$3"
}

prompt_username

while true; do
    prompt_password "enter"
    prev_pw="${password}"
    prompt_password "repeat"

    if [ "$password" = "$prev_pw" ]; then
        save_password "$username" "$password" "$1"
        break
    else
        echo "passwords don't match :("
    fi
done
