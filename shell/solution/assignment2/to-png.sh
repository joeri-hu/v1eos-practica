#!/usr/bin/env bash
find "$1" -name '*.jpg' -exec sh -c 'convert "$1" -resize 128x128\> "${1%.*}.png"' sh {} \;
