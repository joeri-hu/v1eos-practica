#!/usr/bin/env bash
> "$3"
for file in "$1"/*; do
    if eval "$2" "$file"; then
        echo "success: '$2 $file'" >> "$3" 
    else
        echo "failure: '$2 $file'" >> "$3"
    fi
done
