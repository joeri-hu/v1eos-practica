#!/usr/bin/env bash
dest="images"
mkdir -p "$dest"
for file in "$1"/*.jpg; do
    mv "$file" "$dest"
done
find "$1" -name '*.png' -exec mv -i {} "$dest" \;
