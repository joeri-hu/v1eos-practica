#!/usr/bin/env bash

text_to_1337() {
    local input="$*"

    for (( index=0; index < ${#input}; ++index )); do
        letter="${input:$index:1}"

        case "$letter" in
        a|A) output+=4;;
        e|E) output+=3;;
        l|L) output+=1;;
        o|O) output+=0;;
        t|T) output+=7;;
        *)   output+="$letter";;
        esac
    done
}

get_converter() {
    if [ "$1" == "-u" ]; then
        converter="^^"
    else
        converter=",,"
    fi
}

get_converter "$1"

while read line; do
    unset output
    eval text_to_1337 "\${line$converter}"
    echo "${output}"
done
