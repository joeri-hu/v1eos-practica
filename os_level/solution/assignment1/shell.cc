#include <iostream>
#include <algorithm>
#include <iterator>
#include <chrono>
#include <string>
#include <array>
#include <unistd.h>
#include <syscall.h>
#include <wait.h>
#include <fcntl.h>
#include "shell.hh"

auto main() -> int {
    do {
        static auto const prompt = load_command_prompt();
        std::cout << prompt;
        std::string input;
        std::getline(std::cin, input);

        static constexpr auto options = std::array{
            data{"new_file", new_file},
            data{"ls", list},
            data{"src", src},
            data{"find", find},
            data{"seek", seek},
            data{"exit", *[]{std::exit(EXIT_SUCCESS);}},
            data{"quit", *[]{std::exit(EXIT_SUCCESS);}},
            data{"error", *[]{std::exit(EXIT_FAILURE);}}
        };
        std::find_if(options.cbegin(), options.cend(),
            [&input = std::as_const(input)]
            (auto const& option) -> bool {
                return option(input);
            }
        );
    } while (not std::cin.eof());

    return EXIT_SUCCESS;
}

auto load_command_prompt(std::string const& config) -> std::string {
    if (auto const fd = open(config.data(), O_RDONLY, S_IRUSR); fd not_eq -1) {
        auto const max_prompt_size = 16;
        std::array<char, max_prompt_size + 1> buffer;
        auto const br = read(fd, buffer.data(), max_prompt_size);
        close(fd);

        if (br not_eq -1) {
            buffer[br] = '\0';
            return buffer.data();
        }
    }
    auto const default_prompt = "> ";
    return default_prompt;
}

auto new_file() -> void {
    std::cout << "file name: ";
    std::string file;
    std::getline(std::cin, file);

    std::cout << "content:\n";
    auto const input = std::istreambuf_iterator{std::cin};
    auto content = std::string{input, decltype(input){}};
    auto const fd = open(file.data(), O_CREAT | O_WRONLY, S_IRWXU);

    if (fd == -1) {
        std::cerr << "unable to open file :(\n";
        return;
    }
    auto const eof = std::string_view{"<EOF>\n"};

    if (auto const eof_pos = content.find(eof, content.size() - eof.size());
        eof_pos not_eq std::string::npos
    ) {
        content.erase(eof_pos);
    }
    if (auto const bw = write(fd, content.data(), content.size()); bw == -1) {
        std::cerr << "unable to write content to file :(\n";
    }
    close(fd);
}

auto list() -> void {
    fork_process("list (all)", "/bin/ls", "-la");
}

template<typename... Ts>
auto fork_process(std::string_view name, Ts... args) -> void {
    fork_process(name,
        [args...]{execute_command(args...);},
        [](pid_t pid) -> void {wait_for_process(pid);}
    );
}

template<typename Fc, typename Fp>
auto fork_process(
    std::string_view name,
    Fc const& child_action,
    Fp const& parent_action
) -> decltype(child_action(), void()) {
    if (auto const pid = fork(); pid == -1) {
        std::cerr << "unable to fork '" << name << "' process :(\n";
    } else if (pid == 0) {
        child_action();
    } else {
        parent_action(pid);
    }
}

template<typename... Ts>
auto execute_command(std::string const& command, Ts... args) -> void {
    using string_list = char const *[];
    execve_(command.data(),
        string_list{command.data(), args..., nullptr}, nullptr);

    std::cerr << "unable to execute '" << command << "' command :(\n";
    std::exit(EXIT_FAILURE);
}

auto wait_for_process(pid_t pid) -> void {
    if (int wstatus; wait4(pid, &wstatus, 0, nullptr) == -1) {
        std::cerr << "unable to wait for child process '" << pid << "' :(\n";
    } else if (not WIFEXITED(wstatus)) {
        std::cerr << "child process '" << pid << "' exited gracelessly :(\n";
    }
}

auto find() -> void {
    std::cout << "search query: ";
    std::string query;
    std::getline(std::cin, query);
    std::array<int, 2> fd_pair;

    if (pipe(fd_pair.data()) == -1) {
        std::cerr << "unable to create pipe :(\n";
        return;
    }
    using pipe_end = struct {enum {read, write};};
    fork_process("find", [&fd_pair = std::as_const(fd_pair)]{
        close(std::get<pipe_end::read>(fd_pair));
        dup2(std::get<pipe_end::write>(fd_pair), STDOUT_FILENO);
        close(std::get<pipe_end::write>(fd_pair));
        execute_command("/usr/bin/find", ".");
    }, [&](pid_t pid1) -> void {
        fork_process("grep", [&]{
            close(std::get<pipe_end::write>(fd_pair));
            dup2(std::get<pipe_end::read>(fd_pair), STDIN_FILENO);
            close(std::get<pipe_end::read>(fd_pair));
            execute_command("/bin/grep", query.data());
        }, [&fd_pair = std::as_const(fd_pair), pid1](pid_t pid2) -> void {
            close(std::get<pipe_end::read>(fd_pair));
            close(std::get<pipe_end::write>(fd_pair));
            wait_for_process(pid1);
            wait_for_process(pid2);
        });
    });
}

auto seek() -> void {
    constexpr auto markers = 2;
    constexpr auto gap_size = (1 << 20) * 5;
    test_file_io("seek", *[](int fd) -> void {
        if (lseek(fd, gap_size - markers, SEEK_CUR) == -1) {
            std::cerr << "unable to adjust file descriptor :(\n";
        }
    });
    test_file_io("loop", *[](int fd) -> void {
        for (auto count = 0; count < gap_size - markers; ++count) {
            if (write(fd, "\0", 1) == -1) {
                std::cerr << "unable to write filler byte :(\n";
                break;
            }
        }
    });
}

template<typename F>
auto test_file_io(
    std::string const& file,
    F const& action,
    std::string const& marker
) -> void {
    auto const fd = open(file.data(), O_CREAT | O_WRONLY | O_TRUNC, S_IRWXU);

    if (fd == -1) {
        std::cerr << "unable to open '" << file << "' file :(\n";
        return;
    }
    if (auto const bw = write(fd, marker.data(), marker.size()); bw == -1) {
        close(fd);
        std::cerr << "unable to write marker1 to '" << file << "' file :(\n";
        return;
    }
    auto const runtime = measure_runtime([action, fd]{action(fd);});
    auto const bw = write(fd, marker.data(), marker.size());
    close(fd);

    if (bw == -1) {
        std::cerr << "unable to write marker2 to '" << file << "' file :(\n";
        return;
    }
    print_results(file, runtime);    
}

template<typename F, typename T>
auto measure_runtime(F const& action) -> T {
    auto const start = std::chrono::steady_clock::now();
    action();
    auto const end = std::chrono::steady_clock::now();
    return std::chrono::duration<T>(end - start).count();
}

auto print_results(std::string const& file, float runtime) -> void {
    std::cout << "runtime '" << file << "' operation: "
        << std::fixed << runtime << " seconds\n"
        "  file info (rounded): " << std::flush;
    fork_process("list (rounded)", "/bin/ls", "-lh", file.data());
    std::cout << "  file info (real): " << std::flush;
    fork_process("list (real)", "/bin/ls", "-lS", file.data());
}

auto src() -> void {
    auto const fd = open(__FILE__, O_RDONLY, S_IRUSR);

    if (fd == -1) {
        std::cerr << "unable to open '" << __FILE__ << "' file :(\n";
        return;
    }
    auto const buff_size = (1 << 10) * 8;
    std::array<char, buff_size> buffer;

    for (;;) {
        if (auto const br = read(fd, buffer.data(), buff_size); br == -1) {
            std::cerr << "unable to read from '" << __FILE__ << "' file :(\n";
            break;
        } else {
            std::cout << std::string(buffer.data(), br);

            if (br < buff_size) {
                break;
            }
        }
    }
    close(fd);
}
