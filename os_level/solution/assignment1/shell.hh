#ifndef SHELL_HH
#define SHELL_HH

#include <chrono>
#include <string>
#include <unistd.h>
#include <syscall.h>
// #include <wait.h>

using func = void (&)();
using data = struct tag {
    std::string_view const name;
    func action;

    constexpr auto operator()()
    const noexcept -> void {
        action();
    }

    constexpr auto operator()(
        std::string_view input
    ) const noexcept -> bool {
        if (input == name) {
            (*this)();
            return true;
        }
        return false;
    }
};

// in case direct usage of indirect system call is required:
template<auto Nr>
constexpr auto system_call = [](auto... args) {
    return syscall(Nr, args...);
};
// constexpr auto close_ = system_call<SYS_close>;
// constexpr auto dup2_ = system_call<SYS_dup2>;
constexpr auto execve_ = system_call<SYS_execve>;
// constexpr auto fork_ = system_call<SYS_fork>;
// constexpr auto open_ = system_call<SYS_open>;
// constexpr auto pipe_ = system_call<SYS_pipe>;
// constexpr auto read_ = system_call<SYS_read>;
// constexpr auto wait4_ = system_call<SYS_wait4>;
// constexpr auto write_ = system_call<SYS_write>;

auto load_command_prompt(
    std::string const& config = "prompt.cfg"
) -> std::string;

template<typename... Ts>
auto fork_process(std::string_view name, Ts... args) -> void;
template<typename Fc, typename Fp>
auto fork_process(
    std::string_view name,
    Fc const& child_action,
    Fp const& parent_action
) -> decltype(child_action(), void());

template<typename... Ts>
[[noreturn]]
auto execute_command(std::string const& command, Ts... args) -> void;
auto wait_for_process(pid_t pid) -> void;

template<typename F>
auto test_file_io(
    std::string const& file,
    F const& action,
    std::string const& marker = "x"
) -> void;
template<typename F, typename T = float>
auto measure_runtime(F const& action) -> T;
auto print_results(std::string const& file, float runtime) -> void;

auto main() -> int;
auto new_file() -> void;
auto list() -> void;
auto find() -> void;
auto seek() -> void;
auto src() -> void;

#endif
