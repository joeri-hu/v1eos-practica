#include <iostream>
#include <algorithm>
#include <string>

template<typename F>
auto text_to_1337(char& letter, F convert) -> void {
    switch (std::tolower(letter)) {
    case 'e': letter = '3'; break;
    case 'l': letter = '1'; break;
    case 'o': letter = '0'; break;
    case 't': letter = '7'; break;
    default: letter = convert(letter);
    }
}

auto is_uppercase_enabled(int argc, char *argv[]) -> bool {
    return argc == 2
        and argv[1][0] == '-'
        and std::tolower(argv[1][1]) == 'u';
}

auto main(int argc, char *argv[]) -> int {
    for (std::string line; std::getline(std::cin, line);) {
        using convert_t = int (*)(int);
        static auto const converter = is_uppercase_enabled(argc, argv)
            ? convert_t{std::toupper}
            : convert_t{std::tolower};
        std::for_each(line.begin(), line.end(), [](char& letter) -> void {
            text_to_1337(letter, converter);
        });
        std::cout << line << std::endl;
    }
    return 0;
}
