#include <algorithm>
#include <iostream>
#include <string>

template<auto convert>
auto text_to_1337(unsigned char symbol)
-> decltype(convert(symbol)) {
    switch (std::tolower(symbol)) {
    case 'a': return '4';
    case 'e': return '3';
    case 'l': return '1';
    case 'o': return '0';
    case 't': return '7';
    default : return convert(symbol);
    }
}

auto is_uppercase_enabled(int argc, char *argv[]) -> bool {
    return argc == 2
        and argv[1][0] == '-'
        and std::tolower(static_cast<unsigned char>(
            argv[1][1])) == 'u';
}

auto main(int argc, char *argv[]) -> int {
    for (std::string line; std::getline(std::cin, line);) {
        static auto const converter = is_uppercase_enabled(argc, argv)
            ? &text_to_1337<::toupper>
            : &text_to_1337<::tolower>;
        std::transform(line.cbegin(), line.cend(), line.begin(), converter);
        std::cout << line << std::endl;
    }
    return 0;
}
